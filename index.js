const Vec3 = require('vec3').Vec3
let Item

module.exports.server = (serv) => {
    Item = serv.plugins["obsidian-ender-chest"].settings.Item // todo: find a cleaner way to share "prismarine-item"
}

module.exports.player = (player, serv) => {
    const state = {
        open: false
    }

    player.openEnderChest = (location) => {
        const slotCount = 27
        const windowId = 166 // todo: look up the purpose of windowIds

        player._client.write('open_window', {
            windowId,
            inventoryType: 'minecraft:generic_9x3',
            windowTitle: JSON.stringify('Ender Chest'),
            slotCount
        })

        state.open = {
            windowId,
            location
        }

        player[location ? 'playSoundAtSelf' : 'playSound']('block.enderchest.open', {
            volume: 10,
            pitch: 0.01
        })

        player._client.write('window_items', {
            windowId,
            items: new Array(slotCount).fill(
                Item.toNotch(new Item(3, 16))
            )
        })
    }

    player._client.on('block_place', async ({ direction, location } = {}) => {
        if (player.crouching || direction === -1) return

        const reference = new Vec3(location.x, location.y, location.z)
        const block = await player.world.getBlock(reference)

        if (block.name !== 'ender_chest') return

        const blockAbove = await player.world.getBlockType(reference.plus(new Vec3(0, 1, 0)))

        if (blockAbove) return // todo: add proper validation for slabs, stairs etc

        player._client.write('block_action', {
            location,
            byte1: 1, // action id
            byte2: 1, // # of opening the chest - todo: make actual count
            blockId: player.world.getBlockType(reference)
        })

        player.openEnderChest(location)
    })

    player._client.on('close_window', async ({ windowId } = {}) => {
        if (!state.open) return

        const {
            windowId: storedWindowId,
            location
        } = state.open

        if (windowId !== storedWindowId) return

        state.open = false

        player[location ? 'playSoundAtSelf' : 'playSound']('block.enderchest.close', {
            volume: 10,
            pitch: 0.01
        })

        if (!location) return

        const reference = new Vec3(location.x, location.y, location.z)

        player._client.write('block_action', {
            location,
            byte1: 1, // action id
            byte2: 0, // # of opening the chest - todo: make actual count
            blockId: player.world.getBlockType(reference)
        })
    })

    player.commands.add({
        base: 'ec',
        info: 'Open your Ender Chest',
        usage: '/ec',
        action: () => player.openEnderChest() && 'Somehow you have opened your Ender Chest...'
    })
}